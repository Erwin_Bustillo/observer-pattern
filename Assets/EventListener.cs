using UnityEngine.Events;
using UnityEngine;


[System.Serializable]
public class UnityGameobjectEvent : UnityEvent<GameObject> { }

public class EventListener : MonoBehaviour
{

    public Event gEvent;
    public UnityGameobjectEvent response = new UnityGameobjectEvent();

    private void OnEnable()
    {
        gEvent.Register(this);
    }

    private void OnDisable()
    {
        gEvent.Unregister(this);
    }

    public void OnEventOccurs(GameObject go)
    {
        response.Invoke(go);
    }
}
